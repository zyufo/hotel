﻿CREATE TABLE [dbo].[reserve] (
    [RId]                 INT          IDENTITY (1, 1) NOT NULL,
    [check in date]       VARCHAR (50) NULL,
    [check out date]      VARCHAR (50) NULL,
    [Number of Guests]    VARCHAR (50) NULL,
    [Number of rooms]     VARCHAR (50) NULL,
    [credit card]         VARCHAR (50) NULL,
    [name on credit card] VARCHAR (50) NULL,
    [credit card number]  VARCHAR (50) NULL,
    [Expiration Date]     VARCHAR (50) NULL,
    [Username]            VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([RId] ASC)
);

