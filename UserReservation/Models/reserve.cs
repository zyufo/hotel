namespace UserReservation.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("reserve")]
    public partial class reserve
    {
        [Key]
        public int RId { get; set; }

        [Column("check in date")]
        [DisplayName("Check in date")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "This field is required")]
        public DateTime? check_in_date { get; set; }

        [Column("check out date")]
        [DisplayName("Check on date")]
        [DataType(DataType.Date)]
        [DateValidation]
        [Required(ErrorMessage = "This field is required")]
        public DateTime? check_out_date { get; set; }

        [Column("Number of Guests")]
        [DisplayName("Number of guests")]
        [StringLength(50)]
        public string Number_of_Guests { get; set; }

        [Column("Number of rooms")]
        [DisplayName("Number of rooms")]
        [StringLength(50)]
        [Required(ErrorMessage = "This field is required")]
        public string Number_of_rooms { get; set; }

        [Column("credit card")]
        [DisplayName("Credit card type")]
        [StringLength(50)]
        [Required(ErrorMessage = "This field is required")]
        public string credit_card { get; set; }

        [Column("name on credit card")]
        [SpecialCharacterValidation]
        [Required(ErrorMessage = "This field is required")]
        [DisplayName("Name on credit card")]
        [StringLength(50)]
        public string name_on_credit_card { get; set; }

        [Column("credit card number")]
        [CreditCardValidation]
        [DisplayName("Credit card number")]
        [Required(ErrorMessage = "This field is required")]
        [StringLength(50)]
        public string credit_card_number { get; set; }

        [Column("Expiration Date")]
        [DisplayName("Expiration date")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "This field is required")]
        [StringLength(50)]
        public string Expiration_Date { get; set; }

        [StringLength(50)]
        public string Username { get; set; }
    }
}
