namespace UserReservation.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    [Table("User")]
    public partial class User
    {
        public int Id { get; set; }

        [StringLength(50)]
        [DisplayName("User Name")]
        [Required(ErrorMessage = "This field is required")]
        public string Username { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "This field is required")]
        [DisplayName("Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [StringLength(50)]
        [DisplayName("Fist Name")]
        [Required(ErrorMessage = "This field is required")]
        [SpecialCharacterValidation]
        public string Firstname { get; set; }

        [StringLength(50)]
        [DisplayName("Last Name")]
        [Required(ErrorMessage = "This field is required")]
        [SpecialCharacterValidation]
        public string Lastname { get; set; }

        [StringLength(50)]
        [DisplayName("Street")]
        [Required(ErrorMessage = "This field is required")]
        public string Street { get; set; }

        [StringLength(50)]
        [DisplayName("City")]
        [Required(ErrorMessage = "This field is required")]
        [SpecialCharacterValidation]
        public string City { get; set; }

        [StringLength(50)]
        [DisplayName("Province/State")]
        [Required(ErrorMessage = "This field is required")]
        public string Provin { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "This field is required")]
        public string Coun { get; set; }

     
        [StringLength(50)]
        [Display(Name = "Postal Code")]
        [PostalValidate]
        [Required(ErrorMessage = "The postal code is required")]
        public string Postal { get; set; }

        [StringLength(50)]
        [DisplayName("PhoneNumber")]
        [Required(ErrorMessage = "This field is required")]
        public string Phone { get; set; }

        [StringLength(50)]
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

     
    }
}
