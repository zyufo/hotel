namespace UserReservation.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Province")]
    public partial class Province
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string ProvinceName { get; set; }

        [StringLength(50)]
        public string CountryId { get; set; }

        [StringLength(50)]
        public string Countryname { get; set; }

        [StringLength(50)]
        public string ProvinceId { get; set; }
    }
}
