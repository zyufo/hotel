namespace UserReservation.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class LocationEntities : DbContext
    {
        public LocationEntities()
            : base("name=LocationEntities1")
        {
        }

        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Province> Province { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>()
                .Property(e => e.Countryname)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.CountryId)
                .IsUnicode(false);

            modelBuilder.Entity<Province>()
                .Property(e => e.ProvinceName)
                .IsUnicode(false);

            modelBuilder.Entity<Province>()
                .Property(e => e.CountryId)
                .IsUnicode(false);

            modelBuilder.Entity<Province>()
                .Property(e => e.Countryname)
                .IsUnicode(false);

            modelBuilder.Entity<Province>()
                .Property(e => e.ProvinceId)
                .IsUnicode(false);
        }
    }
}
