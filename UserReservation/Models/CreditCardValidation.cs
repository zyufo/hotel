﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using UserReservation.Models;

namespace UserReservation.Models
{
    public class CreditCardValidation : ValidationAttribute
    {
        string str = null;

        private int WhatType(string str)
        {
            if (str.CompareTo("Visa") == 0)
            {
                return (1);
            }
            else if (str.CompareTo("Master Card")==0) { return 2; }
            else return 3;
        }

        protected override ValidationResult IsValid(object value,
                           ValidationContext validationContext)
        {
            reserve reservation = (reserve)validationContext.ObjectInstance;
            String CardNumber = null;
            CardNumber = (String)value;
            String Cardtype = (String)reservation.credit_card;
            int type = 0;
            bool isValidCard = false;
            string pattern = "";
            Regex regex = new Regex(pattern);
            if (CardNumber != null) {
            switch (WhatType(Cardtype)) {
                case 1:
                    pattern = @"^4[0-9]{12}(?:[0-9]{3})?$";
                    regex = new Regex(pattern);
                    isValidCard = regex.IsMatch(CardNumber);
                    if (!isValidCard) { return new ValidationResult("Invalid Visa Number."); }
                    break;
                case 2:
                    pattern = @"^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$";
                    regex = new Regex(pattern);
                    isValidCard = regex.IsMatch(CardNumber);
                    if (!isValidCard) { return new ValidationResult("Invalid Master Card Number."); }
                    break;
                case 3:
                    pattern = @" ^3[47][0-9]{13}$";
                    regex = new Regex(pattern);
                    isValidCard = regex.IsMatch(CardNumber);
                    if (!isValidCard) { return new ValidationResult("Invalid American Express Number."); }
                    break;
            }
            }
            return ValidationResult.Success;
          
        }
    }
}