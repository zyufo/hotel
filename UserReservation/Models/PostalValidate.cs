﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using UserReservation.Models;

namespace UserReservation.Models
{
    public class PostalValidate : ValidationAttribute
    {

        private int WhichCountry(string str)
        {
            if (str.CompareTo("Canada") == 0)
            {
                return (1);
            }
            else { return 2; }
        }

        protected override ValidationResult IsValid(object value,
                           ValidationContext validationContext)
        {
            User user = (User)validationContext.ObjectInstance;
            String ZipCode = null;
            ZipCode = (String)value;
            String Country = (String)user.Coun;
            int type = 0;
            bool isValidCard = false;
            string pattern = "";
            Regex regex = new Regex(pattern);
            if (ZipCode != null)
            {
                switch (WhichCountry(Country))
                {
                    case 1:
                        pattern = @"^([a-zA-Z]\d[a-zA-Z]( )?\d[a-zA-Z]\d)$";
                        regex = new Regex(pattern);
                        isValidCard = regex.IsMatch(ZipCode);
                        if (!isValidCard) { return new ValidationResult("Invalid Canada Zipcode."); }
                        break;
                    case 2:
                        pattern = @"^\d{5}-\d{4}|\d{5}|[A-Z]\d[A-Z] \d[A-Z]\d$";
                        regex = new Regex(pattern);
                        isValidCard = regex.IsMatch(ZipCode);
                        if (!isValidCard) { return new ValidationResult("Invalid US Zipcode."); }
                        break;
                }
            }
            return ValidationResult.Success;
          
        }
    }
}