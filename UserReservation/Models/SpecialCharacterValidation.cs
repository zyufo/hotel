﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using UserReservation.Models;

namespace UserReservation.Models
{
    public class SpecialCharacterValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value,
                           ValidationContext validationContext)
        {
       

            String str = (String)value;
            if (str != null)
            {
                bool isValidCard = false;
            string pattern = "^[a-zA-Z_]*$";
            Regex regex = new Regex(pattern);
            isValidCard = regex.IsMatch(str);
             
            if (!isValidCard)
            { return new ValidationResult("This field should not include special characters ; : ! @ # $ % ^ * + ?  / < > 1 2 3 4 5 6 7 8 9 0."); }
            }
            return ValidationResult.Success;
        }
    }
}