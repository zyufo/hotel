﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using UserReservation.Models;

namespace UserReservation.Models
{
    public class DateValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value,
                           ValidationContext validationContext)
        {
            reserve reservation = (reserve)validationContext.ObjectInstance;

            DateTime checkout = (DateTime)value;
            DateTime checkin = (DateTime)reservation.check_in_date;

            int result = DateTime.Compare(checkout, checkin);

            if (result < 0)
            {
                return new ValidationResult("Check_out_date cannot be earlier than Check_in_date");
            }
            else if (result == 0)
            {
                return new ValidationResult("Check_out_date cannot be the same as Check_in_date");
            }

            return ValidationResult.Success;
        }
    }
}