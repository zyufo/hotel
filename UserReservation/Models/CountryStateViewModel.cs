﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserReservation.Models
{
    public class CountryStateViewModel
    {
        public string CountryId { get; set; }
        public string ProvinceId { get; set; }
        public User user { get; set; }
    }
}