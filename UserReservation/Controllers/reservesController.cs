﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UserReservation.Models;

namespace UserReservation.Controllers
{
    public class reservesController : Controller
    {
        private URModel db = new URModel();

        // GET: reserves
        public ActionResult Index()
        {
            var username = (string)Session["userName"];
            var reservationInfo = (from reser in db.reserve
                                   where reser.Username == username
                                   select reser).ToList();
            return View(reservationInfo);
        }

        // GET: reserves/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            reserve reserve = db.reserve.Find(id);
            if (reserve == null)
            {
                return HttpNotFound();
            }
            return View(reserve);
        }

        // GET: reserves/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: reserves/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RId,check_in_date,check_out_date,Number_of_Guests,Number_of_rooms,credit_card,name_on_credit_card,credit_card_number,Expiration_Date,Username")] reserve reserve)
        {
            if (ModelState.IsValid)
            {
                var username = (string)Session["userName"];
                reserve.Username = username;
                db.reserve.Add(reserve);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(reserve);
        }

        // GET: reserves/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            reserve reserve = db.reserve.Find(id);
            if (reserve == null)
            {
                return HttpNotFound();
            }
            return View(reserve);
        }

        // POST: reserves/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RId,check_in_date,check_out_date,Number_of_Guests,Number_of_rooms,credit_card,name_on_credit_card,credit_card_number,Expiration_Date,Username")] reserve reserve)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reserve).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(reserve);
        }

        // GET: reserves/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            reserve reserve = db.reserve.Find(id);
            if (reserve == null)
            {
                return HttpNotFound();
            }
            return View(reserve);
        }

        // POST: reserves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            reserve reserve = db.reserve.Find(id);
            db.reserve.Remove(reserve);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
