﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserReservation.Models;

namespace test.Controllers
{
    public class LoginController : Controller
    {
        public string useraccount;
        protected override void HandleUnknownAction(string actionName)

        {

            try
            {

                this.View(actionName).ExecuteResult(this.ControllerContext);

            }
            catch (InvalidOperationException ieox)

            {

                ViewData["error"] = "Unknown Action: \"" + Server.HtmlEncode(actionName) + "\"";

                ViewData["exMessage"] = ieox.Message;

                this.View("Error").ExecuteResult(this.ControllerContext);

            }

        }
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Autherize(UserReservation.Models.User user)
        {
            using (URModel _db = new URModel())
            {
                var userdetails = _db.User.Where(x => x.Username == user.Username && x.Password == user.Password).First();
                if (userdetails == null)
                {

                    return RedirectToAction("Error", "Login");
                }
                else {
                    useraccount = userdetails.Username;
                    Session["userName"] = userdetails.Username;
                    Session["userId"] = userdetails.Id;
                    var reservationInfo = (from reser in _db.reserve
                                           where reser.Username == userdetails.Username
                                           select reser).ToList(); 
                    return RedirectToAction("Index","reserves");
                }
            }
            return View();
        }

        public ActionResult Loginout() {
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }

        public ActionResult Signup()
        {
            return View();
        }
    }
}