﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UserReservation.Models;

namespace UserReservation.Controllers
{
    public class UsersController : Controller
    {
        private URModel db = new URModel();
        LocationEntities ldb = new LocationEntities();

        protected override void HandleUnknownAction(string actionName)

        {

            try
            {

                this.View(actionName).ExecuteResult(this.ControllerContext);

            }
            catch (InvalidOperationException ieox)

            {

                ViewData["error"] = "Unknown Action: \"" + Server.HtmlEncode(actionName) + "\"";

                ViewData["exMessage"] = ieox.Message;

                this.View("Error").ExecuteResult(this.ControllerContext);

            }

        }
        // GET: Users
        public ActionResult Index()
        {
            var username = (string)Session["userName"];
            var userInfo = (from us in db.User
                                   where us.Username == username
                                   select us).ToList();
            return View(userInfo);
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var uid = (int)Session["userId"];
            User user = db.User.Find(uid);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            List<Country> CountryList = ldb.Country.ToList();
            ViewBag.CountryList = new SelectList(CountryList,"Countryname","Countryname");
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Username,Password,Firstname,Lastname,Street,City,Provin,Coun,Postal,Phone,Email")] User user)
        {
            List<Country> CountryList = ldb.Country.ToList();
            ViewBag.CountryList = new SelectList(CountryList, "Countryname", "Countryname");
            if (ModelState.IsValid)
            {
                db.User.Add(user);
                db.SaveChanges();
                return RedirectToAction("Success");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Username,Password,Firstname,Lastname,Street,City,Provin,Coun,Postal,Phone,Email")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.User.Find(id);
            db.User.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: Login
        public string useraccount;
        [HttpPost]
        public ActionResult Autherize(UserReservation.Models.User user)
        {
            using (URModel _db = new URModel())
            {
                var userdetails = _db.User.Where(x => x.Username == user.Username && x.Password == user.Password).FirstOrDefault();
                if (userdetails == null)
                {

                    return RedirectToAction("Error", "Users");
                }
                else
                {
                    useraccount = userdetails.Username;
                    Session["userName"] = userdetails.Username;
                    Session["userId"] = userdetails.Id;
                    var reservationInfo = (from reser in _db.reserve
                                           where reser.Username == userdetails.Username
                                           select reser).ToList();
                    return RedirectToAction("Index", "reserves");
                }
            }
            return View();
        }

        public ActionResult Loginout()
        {
            Session.Abandon();
            return RedirectToAction("Login", "Users");
        }

        public ActionResult Signup()
        {
            return View();
        }

        public JsonResult GetStateList(string Country) {
            ldb.Configuration.ProxyCreationEnabled = false;
            List<Province> StateList = ldb.Province.Where(x =>  x.Countryname== Country).ToList();
            return Json(StateList, JsonRequestBehavior.AllowGet);
;        }
    }
}
